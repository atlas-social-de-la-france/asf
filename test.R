# library(remotes)
# install_git(url = "https://gitlab.huma-num.fr/atlas-social-de-la-france/asf", build_vignettes = TRUE)


library(asf)
library(sf)
library(mapsf)
# get_datafile()

# Agregation des donnees d'exemple
dtt <- aggreg_data(data_file = "asf_data/data_csp_2020.csv",
                   tabl_file = "asf_data/tabl_2020.rds",
                   vars = list(tot = "C20_POP15P",
                               va1 = "C20_POP15P_CS1",
                               va2 = "C20_POP15P_CS2",
                               va3 = "C20_POP15P_CS3",
                               va4 = "C20_POP15P_CS4",
                               va5 = "C20_POP15P_CS5",
                               va6 = "C20_POP15P_CS6"),
                   funs = c("sum", "sum", "sum", "sum", "sum", "sum", "sum"),
                   by = c("IRIS", "CODE_IRIS"),
                   maille = "ice")


create_fond(input_path = "asf_data/CONTOURS-IRIS_2-1__SHP__FRA_2020-01-01",
            output_path = "asf_data", 
            year = "2020")

st_layers("asf_data/fond_2020.gpkg")
fond <- st_read("asf_data/fond_2020.gpkg", layer = "fond_2020", quiet = T)


mon_fond <- aggreg_fond(fond_file = "asf_data/fond_2020.gpkg", 
                        tabl_file = "asf_data/tabl_2020.rds",
                        layer = "fond_2020", 
                        maille = "ice")

mon_fond_zoom <- create_zoom(fond = mon_fond, villes = "Besancon",
                             lon = c(-0.574, 5.721, 3.048), lat = c(44.857, 45.182, 50.632), 
                             noms = c("Bordexxaux", "Grenxxoble", "Lixxlle"), 
                             buffer = 10000, k = 15)

ff <- merge_fondata(fond = mon_fond ,
                    zoom = mon_fond_zoom$result,
                    data = dtt,
                    by =  "CODE")

mf_theme("candy")
mf_export(x = ff, filename = "map.png", width = 1000)
mf_map(ff, col = "cornsilk", lwd = .1)
mf_map(ff, "tot", "prop", inches = .05, , border = "grey80", lwd = .1, 
       leg_pos = "bottomright", leg_title = "Population")
mf_label(mon_fond_zoom$labels, "label")
mf_title("La population")
dev.off()


