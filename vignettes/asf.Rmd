---
title: 'asf'
subtitle: "package pour l'Atlas social de la France"
date: '`r Sys.Date()`'
author: "Antoine Beroud"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{asf}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.width = 5,
  fig.height = 6
)
```

## Introduction

L'objectif du package `asf` est d'accompagner les contributrices du projet de l'Atlas social de la France dans la création de cartes et de figures.

Le package vise la création de fonds de cartes harmonisés, enrichis de zooms et créés à partir des contours des iris et contenant les données que les contributrices souhaitent analyser et cartographier.


```{r, echo = FALSE, out.width = "100%", out.height = "100%", fig.cap = "Figure 1 : Objectifs du package"}
knitr::include_graphics("fig/schema_1.png")
```
  
Les différentes fonctions qui composent le package `asf` peuvent être classées ainsi :

* **Préparation des données**  
`create_fond()` permet de lire un dossier "Contours... IRIS" de l'IGN, d'assembler les différentes couches shapefiles qui s'y trouvent (hexagone + 5 DROM) en une seule couche au format .gpkg et de repositionner et redimensionner les DROM en colonne à gauche du fond de carte.  
`create_tabl()` permet de créer une table de passage à l'échelle des iris et complilant des informations nécessaires pour créer les listes de codes d'agrégation qui nous intéressent.  

* **Traitements**  
`aggreg_data()` permet d'agréger les données des contributrices au niveau du maillage souhaité (qui repose sur le code d'agrégation défini dans la fonction `create_tabl()`). Ces données doivent être sous la forme d'un `data.frame` avec une colonne d'identifiants (iris ou commune).  
`aggreg_fond()` permet d'agréger les iris pour construire un fond géographique avec le maillage choisi.  
`create_zoom()` permet de créer des zooms qui se positionnent en dessous du fond de carte principal.  
`simplify_geom()` permet de simplifier les géométries d'une couche.  
`merge_fondata()` permet d'assembler en un seul fichier le fond de carte, les zooms et les données issus des fonctions précédentes.  

* **Statistiques**  
`plot_glis()` permet de calculer les indices de Gini et de ségrégation de plusieurs variables  
`plot_typo()` permet de créer un graphique sur la répartition d'un groupe de variables dans chaque catégorie d'une typologie  
`plot_vars()` permet de créer un graphique sur la répartition de chaque variable dans les catégories d'une typologie 

* **Cartographie**  
`map_q()` permet de créer une carte choroplèthe avec une discrétisation en quantiles.  
`map_ql()` permet de créer une carte des quotients de localisation.  
`map_bi()` permet de créer une carte bivariée à partir de deux variables.


```{r, echo = FALSE, out.width = "100%", out.height = "100%", fig.cap = "Figure 2 : Chaîne de traitements avec les fonctions du package"}
knitr::include_graphics("fig/schema_2.png")
```
  
  
## Préparation des données d'entrée

L'objectif est de créer des fichiers harmonisés directement utilisables par les contributrices en fonction de l'année qui les intéresse. Un fichier géographique avec les iris de l'hexagone et des DROM d'une année donnée et où les DROM sont positionnés à gauche du fond de carte. Un fichier table de correspondance entre les iris de l'hexagone et des DROM d'une année donnée et d'autres niveaux administratifs.


| fichiers                | références                                                                                           |
|-------------------------------|----------------------------------------|
| Couche des IRIS         | IGN, [Contours... IRIS](https://geoservices.ign.fr/contoursiris)                                        |
| Table de correspondance | INSEE, [Table d'appartenance géographique des communes](https://www.insee.fr/fr/information/7671844) |
| Population              | INSEE, [Population en 2020 - Recensement de la population](https://www.insee.fr/fr/statistiques/7631680?sommaire=7632456) |
  
  
## Exemple d'utilisation

On souhaite réaliser plusieurs cartes et graphiques sur la répartition spatiales des différentes CSP en 2020 en France, avec des zooms sur les villes d'Avignon, de Bergerac, de Calais et de Rennes.
  
  
#### 1 | Import du package
```{r, message = FALSE, warning = FALSE}
# Chargement du package 'remotes' pour permettre l'installation depuis Git
library(remotes)

# Installation du package 'asf' depuis le depot GitLab
install_git(url = "https://gitlab.huma-num.fr/atlas-social-de-la-france/asf", build_vignettes = TRUE)

# Chargement du package 'asf'
library(asf)
```
  
  
#### 2 | Import des fichiers d'exemple
```{r, message = FALSE, warning = FALSE}
# Utilisation de la fonction qui permet de telecharger les fichiers d'exemple
eg <- get_example()

# Les deux fichiers pour l'annee 2020 issus des fonctions de preparation des donnees d'entree
fond <- eg$fond
tabl <- eg$tabl

# Le fichier avec les donnees d'exemple que l'on souhaite analyser
data <- eg$data
```
  
  
#### 3 | Création du fond de carte et des zooms
```{r}
# Agregation des iris pour obtenir un melange d'iris, de communes et d'EPCI
fond_aggreg <- aggreg_fond(tabl = tabl,
                           fond = fond,
                           id = c("CODE_IRIS", "CODE_IRIS"),
                           maille = "ice")

# Creation des zooms
zoom_created <- create_zoom(fond = fond_aggreg,
                            villes = c("Avignon", "Bergerac"),
                            lon = c(1.861, -1.679),
                            lat = c(50.951, 48.111),
                            noms = c("Calais","Rennes"),
                            buffer = 10000)

# Recuperation des zooms et de leur nom
zooms <- zoom_created$zooms
labels <- zoom_created$labels

# Simplification des geometries du fond de carte principal
fond_simply <- simplify_geom(fond_aggreg, keep = 0.2)
```
  
  
#### 4 | Agrégation des données d'exemple pour les rendre compatibles avec le fond de carte
```{r}
# Agregation des donnees du fichier d'exemple
data_aggreg <- aggreg_data(tabl = tabl,
                           data = data,
                           vars = c(2:10),
                           funs = c("sum", "sum", "sum", "sum", "sum", "sum", "sum", "sum", "sum"),
                           id = c("CODE_IRIS", "IRIS"),
                           maille = "ice")
```
  
  
#### 5 | Jointure finale entre le fond de carte, les zooms et les données d'exemple
```{r}
# Reunion des trois objets : le fond, les zooms et les donnees
fondata <- merge_fondata(data = data_aggreg, 
                         fond = fond_simply, 
                         zoom = zooms, 
                         id = c("ice", "ice"))
```
  
  
#### 6 | Création d'une carte
```{r}
# Utilisation de ce nouveau fond pour creer une carte
map_bi(fondata, 
       tots = c("C20_POP15P", "C20_POP15P"),
       vars = c("C20_POP15P_CS6", "C20_POP15P_CS3"),
       zoomlabel = labels,
       palette = "rouver")
```
  
Rendu final après un rapide travail sur un DAO
```{r, echo = FALSE, out.width = "100%", out.height = "100%"}
knitr::include_graphics("fig/carte_1.png")
```