#' @title Jointure du fond, des zooms et des donnees
#' @description
#' Cette fonction permet de faire la jointure des trois objets issus des 
#' fonctions aggreg_fond, create_zoom et aggreg_data
#'
#' @param data l'objet data.frame issu de la fonction aggreg_data 
#' @param fond l'objet sf / data.frame issu de la fonction aggreg_fond
#' @param zoom l'objet sf / data.frame issu de la fonction create_zoom
#' @param id le vecteur avec les deux noms des colonnes d'identifiants de 'data'
#' (en premier) et de 'fond' (en second)
#' 
#' @return 
#' La fonction renvoie un objet sf
#' 
#' @examples
#' x <- get_example()
#' tabl <- x$tabl
#' fond <- x$fond
#' data <- x$data
#' data_aggreg <- aggreg_data(tabl = tabl,
#'                            data = data,
#'                            vars = c(2:4),
#'                            funs = c("sum", "sum", "sum"),
#'                            id = c("CODE_IRIS", "IRIS"),
#'                            maille = "EPCI")
#' fond_aggreg <- aggreg_fond(tabl = tabl,
#'                            fond = fond,
#'                            id = c("CODE_IRIS", "CODE_IRIS"),
#'                            maille = "EPCI")
#' zoom_created <- create_zoom(fond = fond, 
#'                             villes = c("Avignon", "Bergerac"))
#' zooms <- zoom_created$zooms
#' merge_fondata(data = data,
#'               fond = fond, 
#'               zoom = zooms,
#'               id = c("EPCI", "EPCI"))
#' @export

merge_fondata <- function(data, 
                          fond, 
                          zoom = NULL,
                          id) {
  
  # Reunion du fond et des zooms s'il y en a
  if (!is.null(zoom)) {
    fond <- rbind(fond, zoom)
    
    # Agregation des geometries des zooms avec ceux du fond de base pour ne pas
    # ajouter d'entites et donc fausser les calculs statistiques
    fond <- stats::aggregate(fond[, attr(fond, "sf_column")], 
                             by = list(fond[[id[2]]]), 
                             FUN = sum, na.rm = TRUE)
    
    colnames(fond)[1] <- id[2]
  }
  
  # Jointure des deux objets issues des fonctions aggreg_data et aggreg_fond
  result <- merge(fond, data, by.x = id[2], by.y = id[1], all.x = TRUE)
  result <- sf::st_cast(result, "MULTIPOLYGON")
  
  return(result)
}