#' @title Creation d'un cartogramme
#' @description
#' Cette fonction permet de creer un cartogramme avec la methode de Dorling
#' 
#' @param fond l'objet sf / data.frame
#' @param var la variable de reference pour la deformation
#' @param min la valeur en dessous de laquelle les entites ne sont pas cartographiees
#' (unite de la variable de reference pour la deformation)
#' @return
#' La fonction renvoie un objet sf et une carte
#' 
#' @examples
#' \dontrun{
#' map_do(fond = fond,
#'        var = "var",
#'        min = 5000)
#' }
#' @export

map_do <- function(fond,
                   var, 
                   min = 5000) {
  
  # Verification des variables specifiees
  if (!all(c(var) %in% names(fond))) {
    stop("La colonne sp\u00e9cifi\u00e9e pour 'var' n'est pas pr\u00e9sente dans 'fond'.")
  }
  
  # Limite pour ne pas conserver les plus petites entites
  fond <- fond[fond[[var]] >= min, ]
  
  # Deformation
  Dorling <- cartogram::cartogram_dorling(fond, 
                               var, 
                               k = 1.75)
  
  # Creation de la carte
  mapsf::mf_map(Dorling)
  
  return(Dorling)
}